import com.zaxxer.hikari.HikariConfig
import org.koin.dsl.module.module

val waggleModule = module (createOnStart = true) {
    single {
        val config = HikariConfig()
        config.driverClassName = "org.postgresql.Driver"
        config.jdbcUrl = "jdbc:postgresql://localhost:5432/postgres"
        config.username = "postgres"
        config.password = "wagglebaby"
        WaggleDataSource(config)
    }

    single { UserRepository(get()) }
    single { WaggleRepository(get()) }
    single { UpgradeRepository(get()) }
    single { WaggleService(get(), get())}
    single { TokenBucketService() }
}

