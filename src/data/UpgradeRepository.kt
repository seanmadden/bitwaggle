import com.sdmadden.bitwaggle.generated.Tables
import com.sdmadden.bitwaggle.generated.tables.pojos.Upgrades
import com.sdmadden.bitwaggle.generated.tables.pojos.UserUpgrades
import org.jooq.impl.DSL.max

class UpgradeRepository(
    private val waggleDataSource: WaggleDataSource
) {

    fun getUpgradeById(upgradeId: Long): Upgrades {
        return waggleDataSource.waggleDb.selectFrom(Tables.UPGRADES)
            .where(Tables.UPGRADES.UPGRADE_ID.eq(upgradeId))
            .fetchOneInto(Upgrades::class.java)
    }

    fun getNextUpgrade(currentUpgrade: Upgrades): Upgrades? {
        return waggleDataSource.waggleDb.selectFrom(Tables.UPGRADES)
            .where(Tables.UPGRADES.UPGRADE_SCALE.gt(currentUpgrade.upgradeScale))
            .orderBy(Tables.UPGRADES.UPGRADE_SCALE)
            .limit(1)
            .fetchOneInto(Upgrades::class.java)
    }

    fun setUserUpgrade(user: User, upgrade: Upgrades) {
        val userUpgrades = waggleDataSource.waggleDb
            .fetchOptional(Tables.USER_UPGRADES, Tables.USER_UPGRADES.USER_ID.eq(user.id))
            .orElse(waggleDataSource.waggleDb.newRecord(Tables.USER_UPGRADES))

        userUpgrades.userId = user.id
        userUpgrades.upgradeId = upgrade.upgradeId
        userUpgrades.store()
    }

    fun getMaxUpgradeScale(): Long {
        return waggleDataSource.waggleDb.select(max(Tables.UPGRADES.UPGRADE_SCALE))
            .from(Tables.UPGRADES)
            .fetchOneInto(Long::class.java)
    }

    fun getCurrentUpgrade(user: User): Upgrades {
        val currentUpgrade = waggleDataSource.waggleDb.selectFrom(Tables.USER_UPGRADES)
            .where(Tables.USER_UPGRADES.USER_ID.eq(user.id))
            .fetchOneInto(UserUpgrades::class.java)

        return if (currentUpgrade == null) {
            val firstUpgrade = waggleDataSource.waggleDb.selectFrom(Tables.UPGRADES)
                .where(Tables.UPGRADES.UPGRADE_SCALE.eq(1))
                .fetchOneInto(Upgrades::class.java)

            waggleDataSource.waggleDb.insertInto(Tables.USER_UPGRADES)
                .set(Tables.USER_UPGRADES.UPGRADE_ID, firstUpgrade.upgradeId)
                .set(Tables.USER_UPGRADES.USER_ID, user.id)
                .execute()

            firstUpgrade
        } else {
            getUpgradeById(currentUpgrade.upgradeId)
        }
    }
}