import com.sdmadden.bitwaggle.generated.Tables
import com.sdmadden.bitwaggle.generated.tables.pojos.Users
import com.sdmadden.bitwaggle.generated.tables.records.UsersRecord
import java.util.*
import kotlin.streams.asSequence

class UserRepository(
    private val waggleDataSource: WaggleDataSource
) {
    fun getUser(name: String): Users? {
        return waggleDataSource.waggleDb.selectFrom(Tables.USERS)
            .where(Tables.USERS.USER_NAME.eq(name))
            .fetchOneInto(Users::class.java)
    }

    fun createUser(name: String): UsersRecord {
        val newUser = waggleDataSource.waggleDb.newRecord(Tables.USERS)
        newUser.userName = name
        val source = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890!@#$^"
        newUser.password = Random().ints(50, 0, source.length)
            .asSequence()
            .map(source::get)
            .joinToString("")

        newUser.store()

        return newUser
    }
}