import com.sdmadden.bitwaggle.generated.Tables
import com.sdmadden.bitwaggle.generated.tables.records.WagsRecord
import org.jooq.impl.DSL.*
import java.util.*

class WaggleRepository(
    private val waggleDataSource: WaggleDataSource
) {
    fun performWag(user: User, amount: Long): WagsRecord {
        val wag = waggleDataSource.waggleDb.newRecord(Tables.WAGS)
        wag.wagId = UUID.randomUUID()
        wag.userId = user.id
        wag.amount = amount
        wag.store()
        return wag
    }

    fun getAvailableBitsForUser(user: User): Long {
        return waggleDataSource.waggleDb
            .select(coalesce(sum(Tables.WAGS.AMOUNT), 0L))
            .from(Tables.WAGS)
            .where(Tables.WAGS.USER_ID.eq(user.id))
            .fetchOneInto(Long::class.java)
    }

    data class ScorePair(val userName: String, val bits: Long)

    fun getTopScores(): List<ScorePair> {
        return waggleDataSource.waggleDb
            .select(
                Tables.USERS.USER_NAME.`as`("userName"),
                coalesce(sum(Tables.WAGS.AMOUNT), 0L).`as`("bits")
            )
            .from(Tables.USERS)
            .leftJoin(Tables.WAGS).on(Tables.WAGS.USER_ID.eq(Tables.USERS.USER_ID))
            .groupBy(Tables.USERS.USER_ID)
            .limit(25)
            .fetchInto(ScorePair::class.java)
    }

    fun countWags(user: User): Long {
        return waggleDataSource.waggleDb
            .select(count())
            .from(Tables.WAGS)
            .where(Tables.WAGS.USER_ID.eq(user.id))
            .fetchSingleInto(Long::class.java)
    }
}