import com.zaxxer.hikari.HikariConfig
import com.zaxxer.hikari.HikariDataSource
import org.jooq.DSLContext
import org.jooq.impl.DSL

class WaggleDataSource(
    private val hikariConfig: HikariConfig
) {

    private val dataSource = makeDataSource()
    val waggleDb: DSLContext = DSL.using(dataSource.connection)

    init {
        waggleDb.execute("SELECT 1")
    }

    private fun makeDataSource(): HikariDataSource {
        hikariConfig.validate()
        return HikariDataSource(hikariConfig)
    }
}