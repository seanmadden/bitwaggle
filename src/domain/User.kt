import io.ktor.auth.Principal

data class User(val name: String, val id: Long) : Principal