import com.sdmadden.bitwaggle.generated.tables.pojos.Upgrades
import com.sdmadden.bitwaggle.generated.tables.records.WagsRecord

class WaggleService(
    private val waggleRepository: WaggleRepository,
    private val upgradeRepository: UpgradeRepository
) {

    fun performWag(user: User): WagsRecord {
        val wagScale = getCurrentUpgrade(user).upgradeScale
        return waggleRepository.performWag(user, 1 * wagScale)
    }

    fun getAvailableBitsForUser(user: User): Long {
        return waggleRepository.getAvailableBitsForUser(user)
    }

    fun getCurrentUpgrade(user: User): Upgrades {
        return upgradeRepository.getCurrentUpgrade(user)
    }

    fun getTopScores(): List<WaggleRepository.ScorePair> {
        return waggleRepository.getTopScores()
    }

    fun upgradeUserAction(user: User): Upgrades {
        val usersBits = getAvailableBitsForUser(user)
        val currentUpgrade = upgradeRepository.getCurrentUpgrade(user)
        val nextUpgrade = upgradeRepository.getNextUpgrade(currentUpgrade)
        if (nextUpgrade != null && usersBits >= nextUpgrade.cost) {
            //TODO: transactionalize this
            waggleRepository.performWag(user, nextUpgrade.cost.times(-1))
            upgradeRepository.setUserUpgrade(user, nextUpgrade)
            return nextUpgrade
        }

        return currentUpgrade
    }
}