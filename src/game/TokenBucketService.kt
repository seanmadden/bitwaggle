import io.github.bucket4j.Bandwidth
import io.github.bucket4j.Bucket
import io.github.bucket4j.Bucket4j
import io.github.bucket4j.Refill
import java.time.Duration
import java.util.concurrent.ConcurrentHashMap

class TokenBucketService : TokenBucketServiceInterface {
    private val userToBucketMap = ConcurrentHashMap<User, Bucket>()

    override fun getBucket(user: User): Bucket {
        var bucket = userToBucketMap[user]
        if (bucket == null) {
            bucket = createNewBucket()
            userToBucketMap[user] = bucket
        }
        return bucket
    }

    private fun createNewBucket() : Bucket {
        val refill = Refill.intervally(10, Duration.ofMinutes(1L))
        val limit = Bandwidth.classic(10, refill)
        return Bucket4j.builder().addLimit(limit).build()
    }

}