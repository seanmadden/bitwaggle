import io.github.bucket4j.Bucket

interface TokenBucketServiceInterface {
    fun getBucket(user: User) : Bucket
}