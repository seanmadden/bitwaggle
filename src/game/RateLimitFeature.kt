import io.github.bucket4j.ConsumptionProbe
import io.ktor.application.*
import io.ktor.auth.Principal
import io.ktor.auth.principal
import io.ktor.http.HttpStatusCode
import io.ktor.response.header
import io.ktor.response.respond
import io.ktor.response.respondBytes
import io.ktor.util.AttributeKey
import io.ktor.util.pipeline.PipelineContext
import io.ktor.util.pipeline.PipelinePhase
import org.koin.standalone.KoinComponent
import org.koin.standalone.inject

class RateLimitFeature(config: Configuration) : KoinComponent {

    private val tokenBucketService by inject<TokenBucketService>()

    class Configuration {

    }

    // Body of the feature
    private fun intercept(context: PipelineContext<Unit, ApplicationCall>) {
        // Add custom header to the response
        val user = context.call.principal<User>()
        if (user != null) {
            val probe = tokenBucketService.getBucket(user).tryConsumeAndReturnRemaining(1)
            context.call.attributes.put(AttributeKey<ConsumptionProbe>("rate-limits"), probe)
//            context.call.response.header("rate-limits-remaining", probe.remainingTokens.toString())
//            context.call.response.header("rate-limits-refresh", probe.nanosToWaitForRefill.toString())
            if (!probe.isConsumed) { // limit is exceeded
                println("OVER THE LIMIT!!!")
//                context.call.respond(HttpStatusCode.TooManyRequests, probe)
            }
        }
    }

    /**
     * Installable feature for [CustomHeader].
     */
    companion object Feature : ApplicationFeature<Application, RateLimitFeature.Configuration, RateLimitFeature> {
        override val key = AttributeKey<RateLimitFeature>("RateLimitFeature")
        private val authenticationPhase = PipelinePhase("Authenticate")
        private val challengePhase = PipelinePhase("Challenge")
        private val rateLimitPhase = PipelinePhase("RateLimit")
        override fun install(pipeline: Application, configure: Configuration.() -> Unit): RateLimitFeature {
            // Call user code to configure a feature
            val configuration = Configuration().apply(configure)

            // Create a feature instance
            val feature = RateLimitFeature(configuration)
            pipeline.insertPhaseBefore(ApplicationCallPipeline.Call, rateLimitPhase)

            pipeline.intercept(rateLimitPhase) {
                feature.intercept(this)
            }

            // Return a feature instance so that client code can use it
            return feature
        }
    }

}