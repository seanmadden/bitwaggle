import io.ktor.application.ApplicationCall
import io.ktor.application.ApplicationCallPipeline
import io.ktor.application.call
import io.ktor.auth.authenticate
import io.ktor.auth.principal
import io.ktor.html.respondHtml
import io.ktor.http.HttpStatusCode
import io.ktor.response.header
import io.ktor.response.respond
import io.ktor.routing.Route
import io.ktor.routing.get
import io.ktor.routing.route
import io.ktor.util.pipeline.PipelineContext
import kotlinx.html.*
import org.koin.ktor.ext.inject
import java.time.Duration


fun Route.waggleGameRoutes() {
    val waggleService: WaggleService by inject()
    val tokenBucketService by inject<TokenBucketService>()

    authenticate("waggleGameAuth") {
        intercept(ApplicationCallPipeline.Call) {
            val principal = getPrincipal(this)
            val probe = tokenBucketService.getBucket(principal).tryConsumeAndReturnRemaining(1)

            call.response.header("X-Requests-Remaining", probe.remainingTokens)

            if (!probe.isConsumed) {
                call.respond(
                    HttpStatusCode.TooManyRequests,
                    mapOf("retryInMs" to Duration.ofNanos(probe.nanosToWaitForRefill).toMillis())
                )
                return@intercept finish()
            }
        }

        route("/waggle") {

            get("/score") {
                val principal = getPrincipal(this)
                val bitsForUser = waggleService.getAvailableBitsForUser(principal)
                val currentUpgrade = waggleService.getCurrentUpgrade(principal)
                call.respond(
                    mapOf(
                        "user" to principal.name,
                        "availableBits" to bitsForUser,
                        "currentUpgradeModifier" to currentUpgrade.upgradeScale
                    )
                )
            }
            get("/wag") {
                val principal = getPrincipal(this)
                val wag = waggleService.performWag(principal)
                call.respond(
                    mapOf(
                        "wagId" to wag.wagId,
                        "amount" to wag.amount
                    )
                )
            }
            get("/upgrade") {
                val principal = getPrincipal(this)
                val upgrade = waggleService.upgradeUserAction(principal)
                call.respond(
                    mapOf(
                        "currentScale" to upgrade.upgradeScale,
                        "upgradeName" to upgrade.upgradeName
                    )
                )
            }
        }
    }
    route("/waggle") {
        get("/top-scores") {
            val scores = waggleService.getTopScores()
            call.respond(scores)
        }
        get("/") {
            val topScores = waggleService.getTopScores()
            call.respondHtml {
                body {
                    h1 { +"Top scores (most bits)" }
                    table {
                        thead {
                            tr {
                                th { +"User" }
                                th { +"Bits" }
                            }
                        }
                        tbody {
                            for (score in topScores) {
                                tr {
                                    td { +score.userName }
                                    td { +score.bits.toString() }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

private fun getPrincipal(context: PipelineContext<Unit, ApplicationCall>): User {
    return context.call.principal()!!
}
