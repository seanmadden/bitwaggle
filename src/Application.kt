import io.ktor.application.*
import io.ktor.response.*
import io.ktor.request.*
import io.ktor.routing.*
import io.ktor.http.*
import kotlinx.html.*
import kotlinx.css.*
import io.ktor.features.*
import org.slf4j.event.*
import io.ktor.auth.*
import com.fasterxml.jackson.databind.*
import io.ktor.jackson.*
import org.koin.ktor.ext.inject
import org.koin.ktor.ext.installKoin

fun main(args: Array<String>): Unit = io.ktor.server.jetty.EngineMain.main(args)

@Suppress("unused") // Referenced in application.conf
@kotlin.jvm.JvmOverloads
fun Application.module(testing: Boolean = false) {
    installKoin(listOf(waggleModule))
    install(DefaultHeaders)

    install(CallLogging) {
        level = Level.INFO
        filter { call -> call.request.path().startsWith("/") }
    }

    install(HSTS) {
        includeSubDomains = true
    }

    val userRepository: UserRepository by inject()

    install(Authentication) {
        basic("waggleGameAuth") {
            realm = "bitwaggle 0.1"
            validate {
                val userEntity = userRepository.getUser(it.name) ?: return@validate null

                if (it.name == userEntity.userName &&
                    it.password == userEntity.password
                ) {
                    User(userEntity.userName, userEntity.userId)
                }
                else
                    null
            }
        }
    }

    install(ContentNegotiation) {
        jackson {
            enable(SerializationFeature.INDENT_OUTPUT)
        }
    }

    routing {
        waggleGameRoutes()
        get("/") {
            call.respondText("HELLO WORLD!", contentType = ContentType.Text.Plain)
        }

        post("/register/{userName}") {
            val userName: String = call.parameters["userName"] ?: return@post call.respond(HttpStatusCode.BadRequest)
            val user = userRepository.createUser(userName)
            call.respond(
                mapOf(
                    "userName" to user.userName,
                    "password" to user.password,
                    "notes" to "MAKE NOTE OF THIS PASSWORD, YOU WILL NOT BE ABLE TO RECOVER IT"
                )
            )
        }

        get("/styles.css") {
            call.respondCss {
                body {
                    backgroundColor = Color.red
                }
                p {
                    fontSize = 2.em
                }
                rule("p.myclass") {
                    color = Color.blue
                }
            }
        }

        install(StatusPages) {
            exception<AuthenticationException> { cause ->
                call.respond(HttpStatusCode.Unauthorized)
            }
            exception<AuthorizationException> { cause ->
                call.respond(HttpStatusCode.Forbidden)
            }

        }
    }

}

class AuthenticationException : RuntimeException()
class AuthorizationException : RuntimeException()

fun FlowOrMetaDataContent.styleCss(builder: CSSBuilder.() -> Unit) {
    style(type = ContentType.Text.CSS.toString()) {
        +CSSBuilder().apply(builder).toString()
    }
}

fun CommonAttributeGroupFacade.style(builder: CSSBuilder.() -> Unit) {
    this.style = CSSBuilder().apply(builder).toString().trim()
}

suspend inline fun ApplicationCall.respondCss(builder: CSSBuilder.() -> Unit) {
    this.respondText(CSSBuilder().apply(builder).toString(), ContentType.Text.CSS)
}
