# Bitwaggle
This is a game in the spirit of cookie clicker, adventure capitalist, and other idle games. The difference is that this game is intended to be played by a machine.

Idle games are constrained by your attention. You must keep upgrading or else your snowball slows down.
If you could write a program to play the game, it would be quite easy and boring. That's why the constraint in this game
is actions per minute. This should make it more interesting than writing a bot to play an idle game.

# Running
```bash
docker run --name bitwaggle-db -e POSTGRES_PASSWORD=wagglebaby \
-p 5432:5432 \
-d postgres

./gradlew run
```