create table users
(
  user_id bigserial not null
    constraint users_pk
      primary key,
  user_name varchar(64) not null,
  password varchar(128) not null
);

create table wags
(
  wag_id uuid not null
    constraint wags_pk
      primary key,
  user_id bigint
    constraint wags_users_user_id_fk
      references users,
  amount bigint not null
);

create index idx_wags_user_id
  on wags (user_id);

create table upgrades
(
  upgrade_id bigserial not null
    constraint upgrades_pk
      primary key,
  upgrade_scale bigint not null,
  upgrade_name varchar(256) not null
);

create table user_upgrades
(
  user_id bigint not null
    constraint user_upgrades_users_user_id_fk
      references users,
  upgrade_id bigint not null
    constraint user_upgrades_upgrades_upgrade_id_fk
      references upgrades
);

create unique index user_upgrades_user_id_upgrade_id_uindex
  on user_upgrades (user_id, upgrade_id);


INSERT INTO upgrades (upgrade_scale, upgrade_name)
values (1, 'basic wag'),
       (10, 'wag better'),
       (100, 'super wag')
